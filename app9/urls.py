from django.urls import path, re_path, include
from .views import *
from django.contrib.auth import views
from django.conf import settings

urlpatterns = [
	path('', search, name="data"),
	# path('', index, name="index"),
	path('login', views.LoginView.as_view(), name="login"),
	path('logout', views.LogoutView.as_view(), {'next_page' : settings.LOGOUT_REDIRECT_URL}, name="login"),
		re_path('google/', include('social_django.urls', namespace='social')),
]
from django.shortcuts import render
from .forms import SearchForm
from django.contrib.auth import authenticate
from django.http import JsonResponse, HttpResponseRedirect
import requests
import json
from django.forms.models import model_to_dict
# from django.views.decorators.csrf import csrf_exempt
from .models import *

# Create your views here.
def search(request):
    response = {}
    response['form'] = SearchForm()
    response['data'] = None
    if request.method == 'POST':
        search = request.POST.get('search')
        URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
        get_json = requests.get(URL).json()
        json_dict = json.dumps(get_json)
        response['data'] = json_dict
        return render(request, "book.html", response)
    else:
        return render(request, "book.html", response)




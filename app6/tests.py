from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from django.utils import timezone
from .forms import formStatus

class Lab6Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)

	def test_using_template(self):
		response = Client().get('/lab-6/')
		self.assertTemplateUsed(response, 'status.html')

	def test_using_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_message(self):
		#Creating a new activity
		new_activity = Status.objects.create(status='apa deh')
            
		#Retrieving all available activity
		counting_all_available_message= Status.objects.all().count()
		self.assertEqual(counting_all_available_message,1)

	def test_form_message_input_has_placeholder_and_css_classes(self):
		form = formStatus()
		self.assertIn('<p><label for="id_status">Status:</label> <input type="text" name="status" class="todo-form-input" placeholder="What is on your mind?" required id="id_status" /></p>', form.as_p())
	
	def test_lab6_post_success_and_render_the_result(self):
		message = 'Hello'
		response_post = Client().post('/lab-6/', {'message': message})
		self.assertEqual(response_post.status_code, 200)\

		response = Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertIn(message,html_response)

	def test_lab_6_showing_all_messages(self):
		status_saya = 'status'
		data_saya = {'message': status_saya}
		post_data_saya = Client().post('/lab-6/', data_saya)
		self.assertEqual(post_data_saya.status_code, 200)
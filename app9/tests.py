from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import search

# Create your tests here.
class Lab6Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/books/')
		self.assertEqual(response.status_code, 200)

	def test_using_template(self):
		response = Client().get('/books/')
		self.assertTemplateUsed(response, 'book.html')

	def test_using_func(self):
		found = resolve('/books/')
		self.assertEqual(found.func, search)


from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from .models import Status
from .forms import formStatus

# Create your views here.
response = {}

def index(request):
    status = Status.objects.all()
    response['status'] = status
    response['form'] = formStatus
    html = 'status.html'
    form = formStatus(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status_Save = Status(status=response['status'])
        status_Save.save()

        status_Save = Status.objects.all()
        response['status'] = status_Save

    return render(request, html, response)

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Lab6Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/lab-8/')
		self.assertEqual(response.status_code, 200)

	def test_using_template(self):
		response = Client().get('/lab-8/')
		self.assertTemplateUsed(response, 'profile.html')

	def test_using_func(self):
		found = resolve('/lab-8/')
		self.assertEqual(found.func, index)
